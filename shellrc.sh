#!/bin/bash
echo '#!/bin/bash' > tbashrc
echo '#!/usr/bin/fish' > tconfig.fish
function usage
{
    echo "Usage: $0 -l [ls | lsd | exa] [-b]"
    exit
}

ls_choice="ls"
has_batt=n

while getopts ":bl:" arg; do
      case "${arg}" in
          b) has_batt=y ;;
          l) ls_choice=${OPTARG} ;;
      esac
done

mkdir -p ~/.config/fish
sudo mkdir -p /root/.config/fish

case ${ls_choice} in

ls)
	echo "alias l='ls -laH --color=always'" >> tbashrc
	echo "alias l='ls -laH --color=always'" >> tconfig.fish
	echo "alias ll='ls -laH --color=always | less -R'" >> tbashrc
	echo "alias ll='ls -laH --color=always | less -R'" >> tconfig.fish
	echo "alias cl='clear && ls -laH --color=always'" >> tbashrc
	echo "alias cl='clear ;and ls -laH --color=always'" >> tconfig.fish
	;;

lsd)
	echo "alias l='lsd -la'" > tbashrc
	echo "alias l='lsd -la'" > tconfig.fish
	echo "alias cl='clear && lsd -la'" >> tbashrc
	echo "alias cl='clear ;and lsd -la'" >> tconfig.fish
	;;

exa)
	echo "alias l='exa -la --color=always'" > tbashrc
	echo "alias l='exa -la --color=always'" > tconfig.fish
	echo "alias ll='exa -la --color=always | less -R'" >> tbashrc
	echo "alias ll='exa -la --color=always | less -R'" >> tconfig.fish
	echo "alias cl='clear && exa -la --color=always'" >> tbashrc
	echo "alias cl='clear ;and exa -la --color=always'" >> tconfig.fish
	;;
*)
	usage
	exit
	;;
esac
echo "alias c='clear'" >> tbashrc
echo "alias c='clear'" >> tconfig.fish

case $has_batt in
y)
	echo "alias batt='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage'" >> tbashrc
	echo "alias batt='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage'" >> tconfig.fish
	;;
esac

echo "" >> tbashrc
echo "" >> tconfig.fish

echo "export PS1='\[\033[0;32m\]\h\[\033[0;37m\]:\[\033[0;33m\]\w\[\033[0;37m\]$ '" >> tbashrc

echo "function fish_prompt" >> tconfig.fish
echo "    set_color A3BE8C" >> tconfig.fish
echo '    printf "$hostname"' >> tconfig.fish
echo "    set_color 8FBCBB" >> tconfig.fish
echo '    printf ":"' >> tconfig.fish
echo "    set_color 88C0D0" >> tconfig.fish
echo '    printf "$(prompt_pwd)"' >> tconfig.fish
echo "    set_color 81A1C1" >> tconfig.fish
echo '    printf "\$ "' >> tconfig.fish
echo "    set_color normal" >> tconfig.fish
echo "end" >> tconfig.fish

echo "alias forecast='curl wttr.in'" >> tbashrc
echo "alias forecast='curl wttr.in'" >> tconfig.fish
echo "alias guitarpro='firejail --net=none wine /home/jon/.wine/drive_c/Program\ Files/Arobas\ Music/Guitar\ Pro\ 8/GuitarPro.exe'" >> tbashrc
echo "alias guitarpro='firejail --net=none wine /home/jon/.wine/drive_c/Program\ Files/Arobas\ Music/Guitar\ Pro\ 8/GuitarPro.exe'" >> tconfig.fish
echo "alias lb='lsblk --output NAME,FSTYPE,MOUNTPOINT,FSUSE%'" >> tbashrc
echo "alias lb='lsblk --output NAME,FSTYPE,MOUNTPOINT,FSUSE%'" >> tconfig.fish
echo "alias ssn='sudo shutdown now'" >> tbashrc
echo "alias ssn='sudo shutdown now'" >> tconfig.fish
echo "alias xclip='xclip -sel clip'" >> tbashrc
echo "alias xclip='xclip -sel clip'" >> tconfig.fish
echo "" >> tconfig.fish
#echo "neofetch" >> tbashrc
#echo "neofetch" >> tconfig.fish

echo 'if [ -z "$DISPLAY" ]; then  # Running tty' >> tbashrc
echo "    :" >> tbashrc
echo "else                        # Running Gui" >> tbashrc
echo "    exec /usr/bin/fish" >> tbashrc
echo "fi" >> tbashrc

rm -f ~/.bashrc
rm -f ~/.config/fish/config.fish
sudo rm -f /root/.bashrc
sudo rm -f /root/.config/fish/config.fish
cp ./tbashrc ~/.bashrc
cp ./tconfig.fish ~/.config/fish/config.fish

sed -i 's/0;32m/0;31m/' ./tbashrc
sed -i 's/]$ /[# /' ./tbashrc
sed -i 's/set_color A3BE8C/set_color BF616A/' ./tconfig.fish
sed -i 's/$ /# /' ./tconfig.fish
sudo mv ./tbashrc /root/.bashrc
sudo mv ./tconfig.fish /root/.config/fish/config.fish
#/usr/bin/bash source ~/.bashrc
